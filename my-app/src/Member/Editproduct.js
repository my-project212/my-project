import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
const endfile = ["png", "jpg", "jpeg", "PNG", "JPG"];

function Editproduct() {
  let params = useParams();
  const [errors, setErrors] = useState({});

  const [picture, setPicture] = useState({});
  let [status, setStatus] = useState("");
  const [imagine, setImagine] = useState([]);
  const [brand, setBrand] = useState({});
  const [category, setCategory] = useState({});
  const [avatarCheckbox, setAvatarCheckbox] = useState([]);

  let token = localStorage.getItem("Token");
  token = JSON.parse(token);

  let config = {
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
  };
  const getproduct = axios.get(
    "http://localhost/laravel/laravel/public/api/user/product/" + params.id,
    config
  );
  const getbrand = axios.get(
    "http://localhost/laravel/laravel/public/api/category-brand"
  );
  const [fixproduct, setFixproduct] = useState({
    name: "",
    price: "",
    category: "",
    brand: "",
    status: "",
    sale: "",
    company: "",
    detail: "",
  });
  const [product, setProduct] = useState({});

  useEffect(() => {
    axios.all([getproduct, getbrand]).then(
      axios.spread((...responses) => {
        setProduct(responses[0].data.data);
        setBrand(responses[1].data.brand);
        setCategory(responses[1].data.category);
        setImagine(responses[0].data.data.image);
        setStatus(responses[0].data.data.status);
        setFixproduct({
          name: responses[0].data.data.name,
          price: responses[0].data.data.price,
          category: responses[0].data.data.id_category,
          brand: responses[0].data.data.id_brand,
          status: responses[0].data.data.status,
          sale: responses[0].data.data.sale,
          company: responses[0].data.data.company_profile,
          detail: responses[0].data.data.detail,
        });
      })
    );
  }, []);
  function handleStatus(e) {
    const name = e.target.name;
    const value = e.target.value;
    setFixproduct((state) => ({ ...state, [name]: value }));
    setStatus(e.target.value);
    renderInput();
  }

  function renderInput() {
    if (status == 2) {
      return (
        <>
          <input
            type="status"
            name="sale"
            min="0"
            max="100"
            style={{ width: "30%", display: "inline-block" }}
            placeholder={product.sale}
            onChange={handleChange}
          />

          <label>%</label>
        </>
      );
    }
  }
  function handleChecked(e) {
    if (e.target.checked) {
      avatarCheckbox.push(e.target.value);
    } else {
      const index = avatarCheckbox.indexOf(e.target.value);
      delete avatarCheckbox[index];
      setAvatarCheckbox(
        avatarCheckbox.filter(function (el) {
          return el != null;
        })
      );
    }
  }
  function handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    setFixproduct((state) => ({ ...state, [name]: value }));
  }
  function handleFile(e) {
    setPicture(e.target.files);
  }
  function handleSubmit(e) {
    e.preventDefault();
    let flag = true;
    let errorsubmit = {};
    if (fixproduct.name == product.name) {
      fixproduct.name = product.name;
    } else {
      fixproduct.name = fixproduct.name;
    }
    if (fixproduct.price == product.price) {
      fixproduct.price = product.price;
    } else {
      fixproduct.price = fixproduct.price;
    }
    if (fixproduct.category == product.id_category) {
      fixproduct.category = product.id_category;
    } else {
      fixproduct.category = fixproduct.category;
    }
    if (fixproduct.brand == product.id_brand) {
      fixproduct.brand = product.id_brand;
    } else {
      fixproduct.brand = fixproduct.brand;
    }
    if (fixproduct.status == product.status) {
      fixproduct.status = product.status;
    } else {
      fixproduct.status = fixproduct.status;
    }
    if (fixproduct.company == product.company_profile) {
      fixproduct.company = product.company_profile;
    } else {
      fixproduct.company = fixproduct.company;
    }
    if (fixproduct.detail == product.detail) {
      fixproduct.detail = product.detail;
    } else {
      fixproduct.detail = fixproduct.detail;
    }
    if (avatarCheckbox == "") {
      flag = false;
      alert("please choose picture you want to remove");
    }
    if (picture == "") {
      flag = false;
      errorsubmit.picture = "Please upload your product's pictures";
    }
    if (picture != "") {
      Object.keys(picture).map((key, index) => {
        const typeoffile = picture[key].type;
        const kindoffile = typeoffile.slice(6, 10);

        const filesize = picture[key].size;
        if (filesize > 1024 * 1024) {
          flag = false;
          alert("Your file is too large! Please choose another file");
        }
        if (!endfile.includes(kindoffile)) {
          flag = false;
          alert("Your file must be 'png', 'jpg', 'jpeg', 'PNG', 'JPG'");
        }
      });
    }
    if (flag) {
      alert("successfully");
      console.log(fixproduct);
      let token = localStorage.getItem("Token");
      token = JSON.parse(token);
      let url =
        "http://localhost/laravel/laravel/public/api/user/edit-product/" +
        product.id;
      let config = {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      let formData = new FormData();
      formData.append("name", fixproduct.name);
      formData.append("price", fixproduct.price);
      formData.append("category", fixproduct.category);
      formData.append("brand", fixproduct.brand);
      formData.append("company", fixproduct.company);
      formData.append("detail", fixproduct.detail);
      formData.append("status", fixproduct.status);
      formData.append("sale", fixproduct.sale);
      Object.keys(picture).map((key, index) => {
        formData.append("file[]", picture[key]);
      });
      avatarCheckbox.map((value) => {
        formData.append("avatarCheckBox[]", value);
      });

      axios.post(url, formData, config).then((res) => {
        console.log(res);
      });
    } else {
      setErrors(errorsubmit);
    }
  }
  return (
    <div className="col-sm-6">
      <div className="signup-form">
        <h2>Edit Products!</h2>
        <form action="#" encType="multipart/from-data" onSubmit={handleSubmit}>
          <input
            type="text"
            name="name"
            value={fixproduct.name}
            onChange={handleChange}
          />
          <input
            type="text"
            name="price"
            value={fixproduct.price}
            onChange={handleChange}
          />
          {category.length && (
            <select name="category" onChange={handleChange}>
              <option value="">-- Please choose category --</option>
              {category.map((value, key) => {
                return (
                  <option
                    selected={product.id_category == value.id}
                    key={value.id}
                    value={value.id}
                  >
                    {value.category}
                  </option>
                );
              })}
            </select>
          )}
          {errors.category}
          {brand.length && (
            <select name="brand" onChange={handleChange}>
              <option value="">-- Please choose brand --</option>
              {brand.map((value, key) => {
                return (
                  <option
                    selected={product.id_brand == value.id}
                    key={value.id}
                    value={value.id}
                  >
                    {value.brand}
                  </option>
                );
              })}
            </select>
          )}
          {errors.brand}

          <select name="status" onChange={handleStatus}>
            <option value="0">Status</option>

            <option selected={fixproduct.status == 1} value="1">
              New
            </option>
            <option selected={fixproduct.status == 2} value="2">
              Sale
            </option>
          </select>
          {renderInput()}
          <input
            type="text"
            name="company"
            value={fixproduct.company}
            onChange={handleChange}
          />
          <input type="file" name="files" multiple onChange={handleFile} />
          {errors.picture}
          <h2>Please choose the pictures that you want to remove</h2>

          {imagine.length &&
            imagine.map((value, key) => {
              return (
                <>
                  <div style={{ display: "inline-block" }}>
                    <lable for={value}>
                      <img
                        key={key}
                        src={
                          "http://localhost/laravel/laravel/public/upload/user/product/" +
                          product.id_user +
                          "/" +
                          value
                        }
                        alt=""
                        style={{ width: "100px" }}
                      />
                    </lable>

                    <input
                      type="checkbox"
                      onChange={handleChecked}
                      value={value}
                    />
                  </div>
                </>
              );
            })}
          <textarea
            name="detail"
            value={fixproduct.detail}
            onChange={handleChange}
          />
          <button type="submit" className="btn btn-default">
            Signup
          </button>
        </form>
      </div>
    </div>
  );
}
export default Editproduct;
