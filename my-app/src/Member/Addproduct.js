import axios from "axios";
import { useEffect, useState } from "react";
const endfile = ["png", "jpg", "jpeg", "PNG", "JPG"];

function Addproduct() {
  const [brand, setBrand] = useState("");

  const [category, setCategory] = useState("");

  let [number, setNumber] = useState("");

  const [errors, setErrors] = useState({});
  const [file, setFile] = useState([]);

  const [products, setProducts] = useState({
    name: "",
    price: "",
    category: "",
    brand: "",
    status: "",
    sale: "",
    company: "",
    detail: "",
  });
  useEffect(() => {
    axios
      .get("http://localhost/laravel/laravel/public/api/category-brand")
      .then((res) => {
        setCategory(res.data.category);
        setBrand(res.data.brand);
      });
  }, []);

  function handlechange(e) {
    const name = e.target.name;
    const value = e.target.value;
    setProducts((state) => ({ ...state, [name]: value }));
  }

  function handleStatus(e) {
    const name = e.target.name;
    const value = e.target.value;
    setProducts((state) => ({ ...state, [name]: value }));
    setNumber(e.target.value);
  }

  function handleFile(e) {
    setFile(e.target.files);
  }

  function renderInput() {
    if (number == 2) {
      return (
        <>
          <input
            type="number"
            name="sale"
            onChange={handlechange}
            min="0"
            max="100"
            style={{ width: "30%", display: "inline-block" }}
          />

          <label>%</label>
        </>
      );
    }
  }

  function handleAddproduct(e) {
    e.preventDefault();
    let errorsubmit = {};
    let flag = true;
    if (products.name == "") {
      flag = false;
      errorsubmit.name = "Please enter your product's name";
    }

    if (products.price == "") {
      flag = false;
      errorsubmit.price = "Please enter your product's price";
    } else {
      products.price = Number(products.price);
    }

    if (products.category == "") {
      flag = false;
      errorsubmit.category = "Please enter your product's category";
    }

    if (products.brand == "") {
      flag = false;
      errorsubmit.brand = "Please enter your product's brand";
    }

    if (products.status == "0" || products.status == 1) {
      products.sale = 0;
    }

    if (products.status == 2 && products.sale == "") {
      flag = false;
      errorsubmit.sale = "Please enter your product's sale";
    }

    if (products.company == "") {
      flag = false;
      errorsubmit.company = "Please enter your product's company";
    }

    if (file == "") {
      flag = false;
      errorsubmit.file = "Please enter your product's pictures";
    }
    if (file.length > 3) {
      flag = false;
      errorsubmit.file = "You can choose only maximum 3 pictures";
    }
    if (file != "") {
      Object.keys(file).map((key, index) => {
        const typeoffile = file[key].type;
        const kindoffile = typeoffile.slice(6, 10);

        const filesize = file[key].size;
        if (filesize > 1024 * 1024) {
          flag = false;
          alert("Your file is too large! Please choose another file");
        }
        if (!endfile.includes(kindoffile)) {
          flag = false;
          alert("Your file must be 'png', 'jpg', 'jpeg', 'PNG', 'JPG'");
        }
      });
    }
    if (products.detail == "") {
      flag = false;
      errorsubmit.detail = "Please enter your detail";
    }
    if (flag) {
      alert("successfully");
      console.log(products.price);
      let token = localStorage.getItem("Token");
      token = JSON.parse(token);
      const url =
        "http://localhost/laravel/laravel/public/api/user/add-product";
      let config = {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      let formData = new FormData();
      formData.append("name", products.name);
      formData.append("price", products.price);
      formData.append("category", products.category);
      formData.append("brand", products.brand);
      formData.append("company", products.company);
      formData.append("detail", products.detail);
      formData.append("status", products.status);
      formData.append("sale", products.sale);
      Object.keys(file).map((key, index) => {
        formData.append("file[]", file[key]);
      });
      axios.post(url, formData, config).then((res) => {
        console.log(res);
      });
    } else {
      setErrors(errorsubmit);
    }
  }

  return (
    <div className="col-sm-6">
      <div className="signup-form">
        <h2>Create Products!</h2>
        <form
          action="#"
          encType="multipart/from-data"
          onSubmit={handleAddproduct}
        >
          <input
            type="text"
            name="name"
            placeholder="Name"
            onChange={handlechange}
          />
          {errors.name}
          <input
            type="text"
            name="price"
            placeholder="Price (Only write the number)"
            onChange={handlechange}
          />
          {errors.price}
          {category.length && (
            <select name="category" onChange={handlechange}>
              <option value="">-- Please choose category --</option>
              {category.map((value, key) => {
                return (
                  <option key={value.id} value={value.id}>
                    {value.category}
                  </option>
                );
              })}
            </select>
          )}
          {errors.category}
          {brand.length && (
            <select name="brand" onChange={handlechange}>
              <option value="">-- Please choose brand --</option>
              {brand.map((value, key) => {
                return (
                  <option key={value.id} value={value.id}>
                    {value.brand}
                  </option>
                );
              })}
            </select>
          )}
          {errors.brand}
          <select name="status" onChange={handleStatus}>
            <option value="0">Status</option>
            <option value="1">New</option>
            <option value="2">Sale</option>
          </select>
          {renderInput()}

          <input
            type="text"
            name="company"
            placeholder="Company profile"
            onChange={handlechange}
          />
          {errors.company}
          <input type="file" name="files" multiple onChange={handleFile} />
          {errors.file}

          <textarea
            name="detail"
            placeholder="Detail"
            onChange={handlechange}
          />
          {errors.detail}
          <button type="submit" className="btn btn-default">
            Signup
          </button>
        </form>
      </div>
    </div>
  );
}
export default Addproduct;
