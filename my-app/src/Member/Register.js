import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const endfile = ["png", "jpg", "jpeg", "PNG", "JPG"];

function Register() {
  const navigate = useNavigate();

  const [input, setInput] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    address: "",
    level: 0,
  });

  const [errors, setErrors] = useState({});

  const [file, setFile] = useState("");
  const [avatar, setAvatar] = useState("");
  function handleChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    setInput((state) => ({ ...state, [name]: value }));
  }
  function handleSubmit(e) {
    e.preventDefault();

    let errorsubmit = {};
    let flag = true;

    if (input.name == "") {
      flag = false;
      errorsubmit.name = "Please write your Name!";
    }
    if (input.email == "") {
      flag = false;
      errorsubmit.email = "Please write your Email!";
    }
    if (input.password == "") {
      flag = false;
      errorsubmit.password = "Please write your Password!";
    }
    if (input.phone == "") {
      flag = false;
      errorsubmit.phone = "Please write your Phone number!";
    }
    if (input.address == "") {
      flag = false;
      errorsubmit.address = "Please write your Address!";
    }

    if (file == "") {
      flag = false;
      errorsubmit.file = "Please choose your Picture!";
    } else {
      const typeoffile = file.type;
      const kindoffile = typeoffile.slice(6, 10);
      if (file.size > 1024 * 1024) {
        flag = false;
        errorsubmit.file = "Your file is too large! Please choose another file";
      }
      if (!endfile.includes(kindoffile)) {
        errorsubmit.file =
          "Your file must be 'png', 'jpg', 'jpeg', 'PNG', 'JPG'";
        flag = false;
      }
    }
    if (flag) {
      const data = {
        name: input.name,
        email: input.email,
        password: input.password,
        phone: input.phone,
        address: input.address,
        avatar: avatar,
        level: 0,
      };

      axios
        .post("http://localhost/laravel/laravel/public/api/register", data)
        .then((res) => {
          if (res.data.errors) {
            setErrors(res.data.errors);
          } else {
            alert("Successfully");
            navigate("/login");
          }
        });
    } else {
      setErrors(errorsubmit);
    }
  }

  function handleFile(e) {
    const file = e.target.files;

    let reader = new FileReader();
    reader.onload = (e) => {
      setAvatar(e.target.result);
      setFile(file[0]);
    };
    reader.readAsDataURL(file[0]);
  }
  return (
    <div class="col-sm-4">
      <div class="signup-form">
        <h2>New User Signup!</h2>
        <form action="#" onSubmit={handleSubmit} encType="multipart/from-data">
          <input
            type="text"
            name="name"
            placeholder="Name"
            onChange={handleChange}
          />
          {errors.name}
          <input
            type="email"
            name="email"
            placeholder="Email Address"
            onChange={handleChange}
          />
          {errors.email}
          <input
            type="password"
            name="password"
            placeholder="Password"
            onChange={handleChange}
          />
          {errors.password}

          <input
            type="text"
            name="phone"
            placeholder="Phone"
            onChange={handleChange}
          />
          {errors.phone}

          <input
            type="text"
            name="address"
            placeholder="Address"
            onChange={handleChange}
          />
          {errors.address}
          <input type="file" onChange={handleFile} />
          {errors.file}
          <button type="submit" class="btn btn-default">
            Signup
          </button>
        </form>
      </div>
    </div>
  );
}
export default Register;
