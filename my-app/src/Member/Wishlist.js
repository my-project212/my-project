import { useEffect, useState } from "react";
function Wishlist() {
  const [listitem, setListitem] = useState("");

  useEffect(() => {
    let wishlist = localStorage.getItem("wishlist");
    wishlist = JSON.parse(wishlist);
    if (wishlist) {
      setListitem(Object.values(wishlist));
    }
  }, []);
  function renderWishlist() {
    if (listitem) {
      return listitem.map((value, key) => {
        let img = value.image;
        img = JSON.parse(img);
        return (
          <tr>
            <td>
              <img
                className="media-object"
                src={
                  "http://localhost/laravel/laravel/public/upload/user/product/" +
                  value.id_user +
                  "/" +
                  img[0]
                }
                alt=""
                style={{ width: 150 }}
              />
            </td>
            <td>
              {value.name}
              <br />
              Wed ID: {value.id}
            </td>
            <td> ${value.price}</td>
            <td class="cart_status">
              <div class="cart_status_button">
                {value.status == 1 && <span>New</span>}
                {value.status == 2 && <span> Sale {value.sale}%</span>}
              </div>
            </td>
          </tr>
        );
      });
    }
  }
  return (
    <section id="cart_items">
      <div class="col-sm-9">
        <div class="table-responsive cart_info">
          <table id="mytable" class="table table-condensed">
            <thead>
              <tr class="cart_menu">
                <td class="image">Item</td>
                <td class="description">Description</td>
                <td class="price">Price</td>
                <td class="quantity">Status</td>
                <td></td>
              </tr>
            </thead>
            <tbody>{renderWishlist()}</tbody>
          </table>
        </div>
      </div>
    </section>
  );
}
export default Wishlist;
