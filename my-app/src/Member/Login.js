import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
function Login() {
  const navigate = useNavigate();

  const [inputlogin, setInputlogin] = useState({
    email: "",
    password: "",
    level: 0,
  });

  const [errors, setErrors] = useState({});

  function handleInputlogin(e) {
    const name = e.target.name;
    const value = e.target.value;
    setInputlogin((state) => ({ ...state, [name]: value }));
  }
  function handleSubmitlogin(e) {
    e.preventDefault();
    let errorsubmit = {};
    let truth = true;

    if (inputlogin.email == "") {
      truth = false;
      errorsubmit.email = "Please enter your Email!";
    }
    if (inputlogin.password == "") {
      truth = false;
      errorsubmit.password = "Please enter your Password!";
    }
    if (truth) {
      const datalogin = {
        email: inputlogin.email,
        password: inputlogin.password,
        level: 0,
      };
      axios
        .post("http://localhost/laravel/laravel/public/api/login", datalogin)
        .then((res) => {
          if (res.data.errors) {
            setErrors(res.data.errors);
          } else {
            console.log("OK");
            console.log(res);
            alert("Sucessfully");
            navigate("/");
            let isloggin = true;
            localStorage.setItem("Login", JSON.stringify(isloggin));
            let auth = res.data.Auth;
            localStorage.setItem("Auth", JSON.stringify(auth));
            let token = res.data.success.token;
            localStorage.setItem("Token", JSON.stringify(token));
          }
        });
    } else {
      setErrors(errorsubmit);
    }
  }

  return (
    <div class="col-sm-3 col-sm-offset-1">
      <div class="login-form">
        <h2>Login to your account</h2>
        <form action="#" onSubmit={handleSubmitlogin}>
          <input
            type="text"
            name="email"
            placeholder="Email Address"
            onChange={handleInputlogin}
          />
          {errors.email}
          <input
            type="password"
            name="password"
            placeholder="Password"
            onChange={handleInputlogin}
          />
          {errors.password}
          {errors.errors}
          <br />
          <span>
            <input type="checkbox" class="checkbox" />
            Keep me signed in
          </span>
          <button type="submit" class="btn btn-default">
            Login
          </button>
        </form>
      </div>
    </div>
  );
}
export default Login;
