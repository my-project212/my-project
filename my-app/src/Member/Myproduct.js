import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function Myproduct() {
  const [products, setProducts] = useState({});

  let token = localStorage.getItem("Token");
  token = JSON.parse(token);
  let config = {
    headers: {
      Authorization: "Bearer " + token,
      "Content-Type": "application/x-www-form-urlencoded",
      Accept: "application/json",
    },
  };
  useEffect(() => {
    axios
      .get(
        "http://localhost/laravel/laravel/public/api/user/my-product",
        config
      )
      .then((res) => {
        setProducts(res.data.data);
      });
  }, []);
  function renderProduct() {
    if (products) {
      return Object.keys(products).map((key, index) => {
        let imagine = products[key].image;
        imagine = JSON.parse(imagine);

        return (
          <tr>
            <td>{products[key].id}</td>
            <td>{products[key].name}</td>

            <td>
              {imagine.map((value, i) => (
                <img
                  key={i}
                  src={
                    "http://localhost/laravel/laravel/public/upload/user/product/" +
                    products[key].id_user +
                    "/" +
                    value
                  }
                  alt=""
                  style={{ width: "100px" }}
                />
              ))}
            </td>
            <td>${products[key].price}</td>
            <td>
              <Link to={"/account/my-product/edit-product/" + products[key].id}>
                <i class="far fa-edit"></i>
              </Link>

              <a id={products[key].id} href="#" onClick={handleDelete}>
                <i class="fas fa-window-close" style={{ padding: "20px" }}></i>
              </a>
            </td>
          </tr>
        );
      });
    }
  }

  function handleDelete(e) {
    e.preventDefault();
    const id = e.currentTarget.id;
    axios
      .get(
        "http://localhost/laravel/laravel/public/api/user/delete-product/" + id,
        config
      )
      .then((res) => {
        setProducts(res.data.data);
      });
  }

  return (
    <section id="cart_items" className="col-sm-12">
      <div class="container">
        <div class="table-responsive cart_info">
          <table id="mytable" class="table table-condensed">
            <thead>
              <tr class="cart_menu">
                <td>ID</td>
                <td>Name</td>
                <td class="image">Image</td>
                <td class="price">Price</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody>{renderProduct()}</tbody>
          </table>
          <a type="submit" class="btn btn-default" style={{ float: "right" }}>
            <Link to="/account/my-product/add-product">ADD NEW</Link>
          </a>
        </div>
      </div>
    </section>
  );
}
export default Myproduct;
