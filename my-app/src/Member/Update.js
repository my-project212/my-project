import axios from "axios";
import { useEffect, useState } from "react";

const endfile = ["png", "jpg", "jpeg", "PNG", "JPG"];

function Update() {
  const [update, setUpdate] = useState({
    name: "",
    email: "",
    password: "",
    phone: "",
    address: "",
    level: 0,
  });
  let auth = localStorage.getItem("Auth");
  auth = JSON.parse(auth);
  useEffect(() => {
    setUpdate({
      name: auth.name,
      email: auth.email,
      password: "",
      phone: auth.phone,
      address: auth.address,
      level: 0,
    });
  }, []);

  const [errors, setErrors] = useState({});

  let [updatefile, setUpdatefile] = useState("");
  const [avatar, setAvatar] = useState("");

  function changeupdate(e) {
    const name = e.target.name;
    const value = e.target.value;
    setUpdate((state) => ({ ...state, [name]: value }));
  }
  function handleupdate(e) {
    e.preventDefault();
    let token = localStorage.getItem("Token");
    token = JSON.parse(token);
    let flag = true;
    let errorsubmit = {};

    if (update.name == auth.name) {
      update.name = auth.name;
    } else {
      update.name = update.name;
    }

    if (update.phone == auth.phone) {
      update.phone = auth.phone;
    } else {
      update.phone = update.phone;
    }

    if (update.address == auth.address) {
      update.address = auth.address;
    } else {
      update.address = update.address;
    }

    if (updatefile == "") {
      updatefile = auth.avatar;
    } else {
      const typeoffile = updatefile.type;
      const kindoffile = typeoffile.slice(6, 10);
      if (updatefile.size > 1024 * 1024) {
        flag = false;
        errorsubmit.updatefile =
          "Your file is too large! Please choose another file";
      }
      if (!endfile.includes(kindoffile)) {
        errorsubmit.updatefile =
          "Your file must be 'png', 'jpg', 'jpeg', 'PNG', 'JPG'";
        flag = false;
      }
    }

    if (flag) {
      const formData = new FormData();
      formData.append("name", update.name);
      formData.append("email", update.email);
      formData.append("password", update.password);
      formData.append("phone", update.phone);
      formData.append("address", update.address);
      formData.append("avatar", avatar);
      formData.append("level", 0);

      let config = {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };
      let url =
        "http://localhost/laravel/laravel/public/api/user/update/" + auth.id;
      axios.post(url, formData, config).then((res) => {
        alert("Successfully");
        let auth = res.data.Auth;
        localStorage.setItem("Auth", JSON.stringify(auth));
      });
    } else {
      setErrors(errorsubmit);
    }
  }

  function handlefile(e) {
    const updatefile = e.target.files;
    let reader = new FileReader();
    reader.onload = (e) => {
      setAvatar(e.target.result);
      setUpdatefile(updatefile[0]);
    };
    reader.readAsDataURL(updatefile[0]);
  }
  return (
    <div class="col-sm-7">
      <div class="signup-form">
        <h2>User Update</h2>
        <form action="#" encType="multipart/from-data" onSubmit={handleupdate}>
          <input
            type="text"
            name="name"
            value={update.name}
            onChange={changeupdate}
          />
          <input
            type="text"
            disabled
            name="email"
            readonly
            value={update.email}
            onChange={changeupdate}
          />
          <input
            type="password"
            name="password"
            value={update.password}
            placeholder="Password (Optional)"
            onChange={changeupdate}
          />

          <input
            type="text"
            name="phone"
            value={update.phone}
            onChange={changeupdate}
          />

          <input
            type="text"
            name="address"
            value={update.address}
            onChange={changeupdate}
          />
          <input type="file" onChange={handlefile} />
          {errors.updatefile}
          <button type="submit" class="btn btn-default">
            Signup
          </button>
        </form>
      </div>
    </div>
  );
}

export default Update;
