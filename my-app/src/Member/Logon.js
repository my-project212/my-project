import Login from "./Login";
import Register from "./Register";
function Logon() {
  return (
    <>
      <Login />
      <div class="col-sm-1">
        <h2 class="or">OR</h2>
      </div>
      <Register />
    </>
  );
}
export default Logon;
