import React, { useContext, useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";

function Head() {
  const navigate = useNavigate();
  const context = useContext(UserContext);
  const [number, setNumber] = useState("");
  const [list, setList] = useState("");
  useEffect(() => {
    let item = localStorage.getItem("Item");
    item = JSON.parse(item);
    let total = 0;
    Object.keys(item).map((key, index) => {
      total = total + item[key];
      setNumber(total);
    });

    let wishlist = localStorage.getItem("wishlist");
    wishlist = JSON.parse(wishlist);
    if (wishlist) {
      const arr = Object.keys(wishlist);
      setList(arr.length);
    }
  });
  function renderContext() {
    if (number != "") {
      return (
        <li>
          <a href="checkout.html">
            <i class="fa-solid fa-cart-shopping">
              <Link to="cart"> Cart:{number}</Link>
            </i>
          </a>
        </li>
      );
    }
  }
  function renderLogin() {
    let isloggin = localStorage.getItem("Login");
    if (isloggin) {
      isloggin = JSON.parse(isloggin);
      return (
        <li onClick={logout}>
          <a id="cart" href="">
            <i className="fa fa-lock"></i>Logout
          </a>
        </li>
      );
    } else {
      return (
        <li>
          <a href="">
            <i className="fa fa-shopping-cart"></i>
            <Link to="/Logon">Login</Link>
          </a>
        </li>
      );
    }
  }

  function logout() {
    window.localStorage.removeItem("Login");
    navigate("/logon");
  }

  return (
    <header id="header">
      <div className="header_top">
        <div className="container">
          <div className="row">
            <div className="col-sm-6">
              <div className="contactinfo">
                <ul className="nav nav-pills">
                  <li>
                    <a href="#">
                      <i className="fa fa-phone"></i> +2 95 01 88 821
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-envelope"></i> info@domain.com
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="social-icons pull-right">
                <ul className="nav navbar-nav">
                  <li>
                    <a href="#">
                      <i className="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-linkedin"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-dribbble"></i>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i className="fa fa-google-plus"></i>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="header-middle">
        <div className="container">
          <div className="row">
            <div className="col-md-4 clearfix">
              <div className="logo pull-left">
                <a href="index.html">
                  <img src="images/home/logo.png" alt="" />
                </a>
              </div>
              <div className="btn-group pull-right clearfix">
                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-default dropdown-toggle usa"
                    data-toggle="dropdown"
                  >
                    USA
                    <span className="caret"></span>
                  </button>
                  <ul className="dropdown-menu">
                    <li>
                      <a href="">Canada</a>
                    </li>
                    <li>
                      <a href="">UK</a>
                    </li>
                  </ul>
                </div>

                <div className="btn-group">
                  <button
                    type="button"
                    className="btn btn-default dropdown-toggle usa"
                    data-toggle="dropdown"
                  >
                    DOLLAR
                    <span className="caret"></span>
                  </button>
                  <ul className="dropdown-menu">
                    <li>
                      <a href="">Canadian Dollar</a>
                    </li>
                    <li>
                      <a href="">Pound</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-8 clearfix">
              <div className="shop-menu clearfix pull-right">
                <ul className="nav navbar-nav">
                  <li>
                    <a href="">
                      <i className="fa fa-user"></i>
                      <Link to="/account/update">Account</Link>
                    </a>
                  </li>
                  <li>
                    <a href="">
                      <i className="fa fa-star"></i>
                      <Link to="/account/my-wishlist">
                        Wishlist:
                        {list && list}
                        {list == "" && 0}
                      </Link>
                    </a>
                  </li>
                  {renderContext()}

                  {renderLogin()}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="header-bottom">
        <div className="container">
          <div className="row">
            <div className="col-sm-9">
              <div className="navbar-header">
                <button
                  type="button"
                  className="navbar-toggle"
                  data-toggle="collapse"
                  data-target=".navbar-collapse"
                >
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
              </div>
              <div className="mainmenu pull-left">
                <ul className="nav navbar-nav collapse navbar-collapse">
                  <li>
                    <a href="" className="active">
                      <Link to="/">Home</Link>
                    </a>
                  </li>
                  <li className="dropdown">
                    <a href="#">
                      Shop<i className="fa fa-angle-down"></i>
                    </a>
                    <ul role="menu" className="sub-menu">
                      <li>
                        <a href="">
                          <Link to="/account/my-product">Products</Link>
                        </a>
                      </li>

                      <li>
                        <a href="">
                          <Link to="cart">Cart</Link>{" "}
                        </a>
                      </li>
                      {/* <li>
                        <Link to="/login">Login</Link>
                      </li> */}
                      {renderLogin()}
                    </ul>
                  </li>
                  <li className="dropdown">
                    <a href="#">
                      Blog<i className="fa fa-angle-down"></i>
                    </a>
                    <ul role="menu" className="sub-menu">
                      <li>
                        <a href="">
                          <Link to="Blog">Blog List</Link>
                        </a>
                      </li>
                      <li>
                        <a href="blog-single.html">
                          <Link to="/blog/detail/4">Blog Single</Link>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="404.html">404</a>
                  </li>
                  <li>
                    <a href="contact-us.html">Contact</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-sm-3">
              <div className="search_box pull-right">
                <input type="text" placeholder="Search" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Head;
