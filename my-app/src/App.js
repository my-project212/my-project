import React, { useState } from "react";
import { useLocation } from "react-router-dom";
import "./App.css";
import Footer from "./layout/Footer";
import Head from "./layout/Head";
import Menuacc from "./layout/Menuacc";
import Menuleft from "./layout/Menuleft";
import { UserContext } from "./UserContext";
function App(props) {
  let params1 = useLocation();

  const [number, setNumber] = useState("");
  const [sub, setSub] = useState("");
  function cartContext(data) {
    setNumber(data);
  }
  function moving(data) {
    setSub(data);
  }
  return (
    <UserContext.Provider
      value={{ number, cartContext: cartContext, sub, moving: moving }}
    >
      <Head />
      <section>
        <div className="container">
          <div className="row">
            {params1["pathname"].includes("account") ? (
              <Menuacc />
            ) : (
              <Menuleft />
            )}
            {props.children}
          </div>
        </div>
      </section>
      <Footer />
    </UserContext.Provider>
  );
}

export default App;
