import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import "@fortawesome/fontawesome-free/css/all.min.css";

import App from "./App";
import Blog from "./Blog/Blog";
import Blogdetail from "./Blog/Blogdetail";
import "./index.css";
import Addproduct from "./Member/Addproduct";
import Editproduct from "./Member/Editproduct";
import Logon from "./Member/Logon";
import Myproduct from "./Member/Myproduct";
import Update from "./Member/Update";
import Wishlist from "./Member/Wishlist";
import Cart from "./Product/Cart";
import Home from "./Product/Home";
import ProductDetail from "./Product/ProductDetail";
import reportWebVitals from "./reportWebVitals";
const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <Router>
      <App>
        <Routes>
          <Route index path="/" element={<Home />} />
          <Route path="Blog" element={<Blog />} />
          <Route path="/blog/detail/:id" element={<Blogdetail />} />
          <Route path="/logon" element={<Logon />} />
          <Route path="/account/update" element={<Update />} />
          <Route path="/account/my-product" element={<Myproduct />} />
          <Route
            path="/account/my-product/add-product"
            element={<Addproduct />}
          />
          <Route
            path="/account/my-product/edit-product/:id"
            element={<Editproduct />}
          />
          <Route path="/product/detail/:id" element={<ProductDetail />} />
          <Route path="cart" element={<Cart />} />
          <Route path="/account/my-wishlist" element={<Wishlist />} />
        </Routes>
      </App>
    </Router>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
