import axios from "axios";
import { useState } from "react";
import { useParams } from "react-router-dom";
function Comment(props){
    let params=useParams()
    const [cmt, setCmt]=useState("")
    const id_con=props.id_con
    function handleComment(e){
       
        setCmt(e.target.value)
    }
    
    function handleSubmit(e){
        e.preventDefault();
        let isloggin=localStorage.getItem("Login")
        let auth=localStorage.getItem("Auth")
        let token=localStorage.getItem("Token")
        if(isloggin){
            isloggin=JSON.parse(isloggin)
            auth=JSON.parse(auth)
            token=JSON.parse(token)
                
            const userData=auth
            let url="http://localhost/laravel/laravel/public/api/blog/comment/" + params.id
           

            let config = { 
                headers: { 
                'Authorization': 'Bearer '+ token,
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json'
                } 
            };		
		
           
            if(cmt==""){
                alert("Please write your Message")
            }else{
                const formData=new FormData();
                    formData.append('id_blog',params.id)
                    formData.append('id_user',userData.id)
                    formData.append('id_comment', id_con ? id_con:0)
                    formData.append('comment',cmt)
                    formData.append('image_user',userData.avatar)
                    formData.append('name_user',userData.name)
                   

                axios.post(url,formData,config)
                .then(res=>{
                   props.getcmt(res.data.data)
                   
                    
                })
                
            }
            
        }else{
            alert("Please login")
        }

       
    }   

    
    return(
        <div className="replay-box">
			<div className="row">
				<div className="col-sm-12" id="cmt">
					<h2>Leave a replay</h2>
								
					<div className="text-area" >
						<div className="blank-arrow">
							<label>Your Name</label>
						</div>
						<span>*</span>
                        <form action="" onSubmit={handleSubmit} >
                            <textarea name="message" rows="11" onChange={handleComment}/>
                        
                            <button type="submit" className="btn btn-primary">Post comment</button>

                        </form>
					</div>
				</div>
			</div>
            
		</div>
        
    )
}

export default Comment;