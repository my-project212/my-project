function Listcomment(props) {
  const comments = props.comments;
  function renderComment() {
    return (
      comments &&
      comments.map((value, key) => {
        if (value.id_comment == 0) {
          return (
            <>
              <li className="media" key={value.id}>
                <a className="pull-left">
                  <img
                    className="media-object"
                    src={
                      "http://localhost/laravel/laravel/public/upload/user/avatar/" +
                      value.image_user
                    }
                    alt=""
                    style={{ width: 50 }}
                  />
                </a>
                <div className="media-body">
                  <ul className="sinlge-post-meta">
                    <li>
                      <i className="fa fa-user"></i>
                      {value.name_user}
                    </li>
                    <li>
                      <i className="fa fa-clock-o"></i>{" "}
                      {value.created_at.slice(10, 19)}
                    </li>
                    <li>
                      <i className="fa fa-calendar"></i>{" "}
                      {value.created_at.slice(0, 10)}
                    </li>
                  </ul>
                  <p>{value.comment}</p>

                  <a
                    id={value.id}
                    className="btn btn-primary"
                    href="#cmt"
                    onClick={getId}
                  >
                    <i className="fa fa-reply"></i>Replay
                  </a>
                </div>
                {comments.map((value1) => {
                  if (value.id === +value1.id_comment) {
                    return (
                      <>
                        <li className="media second-media" key={value1.id}>
                          <a className="pull-left" href="#">
                            <img
                              className="media-object"
                              src={
                                "http://localhost/laravel/laravel/public/upload/user/avatar/" +
                                value1.image_user
                              }
                              alt=""
                              style={{ width: 50 }}
                            />
                          </a>
                          <div className="media-body">
                            <ul className="sinlge-post-meta">
                              <li>
                                <i className="fa fa-user"></i>
                                {value1.name_user}
                              </li>
                              <li>
                                <i className="fa fa-clock-o"></i>{" "}
                                {value1.created_at.slice(10, 19)}
                              </li>
                              <li>
                                <i className="fa fa-calendar"></i>{" "}
                                {value1.created_at.slice(0, 10)}
                              </li>
                            </ul>
                            <p>{value1.comment}</p>
                          </div>
                        </li>
                      </>
                    );
                  }
                })}
              </li>
            </>
          );
        }
      })
    );
  }

  function getId(e) {
    props.getreplay(e.target.id);
  }

  return (
    <div className="response-area">
      <h2> RESPONSES</h2>
      <ul className="media-list">{renderComment()}</ul>
    </div>
  );
}

export default Listcomment;
