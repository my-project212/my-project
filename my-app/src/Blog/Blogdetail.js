import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import StarRatings from 'react-star-ratings';
import Comment from "./Comment";
import Listcomment from "./Listcomment";
import Rate from "./Rate";


function Blogdetail() {
    let params=useParams()
    const [data, setData]=useState('')
    const [comments, setComments]=useState([])
    const [id_con, setId_con]=useState('')
    useEffect(()=>{
        axios.get("http://localhost/laravel/laravel/public/api/blog/detail/" + params.id)
        .then(res=>{
            setData(res.data.data)
            setComments(res.data.data.comment)
          
        })
        .catch(error=>console.log(error))
    },[])
    
    function renderDetail(){
       if(data){
            return(
                <div class="single-blog-post">
                    <h3>{data.title}</h3>
                    <div class="post-meta">
                        <ul>
                            <li><i class="fa fa-user"></i> Mac Doe</li>
                            <li><i class="fa fa-clock-o"></i> {data.created_at.slice(10,19)}</li>
                            <li><i class="fa fa-calendar"></i> {data.created_at.slice(0,10)}</li>
                        </ul>
                        <span>
                            <StarRatings
                           
                            starRatedColor="blue"
                            
                            numberOfStars={5}
                            name='rating'
                            />
                        </span> 
                    </div>
                    <a href="">
                        <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + data.image} alt=""/>
                    </a>
                    <p>
                    {data.description}</p> <br/>

                    <p>
                        {data.content}</p> <br/>

                    <p>
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p> <br/>

                    <p>
                        Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.
                    </p>
                    <div class="pager-area">
                        <ul class="pager pull-right">
                            <li><a href="#">Pre</a></li>
                            <li><a href="#">Next</a></li>
                        </ul>
                    </div>
                   
                </div>
                
            )
       }
    }    
   
    function getcmt(data){
       setComments([...comments,data])
       
    }
    
    function getreplay(data){
        setId_con(data)
    }
    
    return (
        
        <div class="col-sm-9">
            <div class="blog-post-area">
                <h2 class="title text-center">Latest From our Blog</h2>
                {renderDetail()}
               
            </div>
            <Rate/>
            <Listcomment comments={comments} getreplay={getreplay}/>
            <Comment getcmt={getcmt} id_con={id_con}/>
        </div>
        
       
    )
}
  
  export default Blogdetail;