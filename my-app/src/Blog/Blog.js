import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Blog() {

    const [data, setData]=useState([])
    useEffect(()=>{

        
        axios.get("http://localhost/laravel/laravel/public/api/blog")
        .then(res=>{
            setData(res.data.blog.data)

        })
        .catch(error=>console.log(error))

    },[])
    function renderData(){
        if(Object.keys(data).length>0){

            return data.map((value, key)=>{
                return (

                    <div className="single-blog-post" key={key}>
                        <h3>{value.title}</h3>
                        <div className="post-meta">
                            <ul>
                                <li><i className="fa fa-user"></i> Mac Doe</li>
                                <li><i className="fa fa-clock-o"></i> {value.created_at.slice(10,19)}</li>
                                <li><i className="fa fa-calendar"></i> {value.created_at.slice(0,10)}</li>
                            </ul>
                            <span>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star"></i>
                                    <i className="fa fa-star-half-o"></i>
                            </span>
                        </div>
                        <a href="">
                            <img src={"http://localhost/laravel/laravel/public/upload/Blog/image/" + value.image} alt=""/>
                        </a>
                        <p>{value.description}</p>
                        <Link  className="btn btn-primary" to={"/blog/detail/" + value.id} >Read More</Link>
                    </div>
                )
            })
        }
    }

    return (
        <div className="col-sm-9">
            <div className="blog-post-area">
                <h2 className="title text-center">Latest From our Blog</h2>
                {renderData()}
            </div>
        </div>
    )
  }
  
  export default Blog;