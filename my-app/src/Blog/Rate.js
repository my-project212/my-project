import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import StarRatings from "react-star-ratings";

// class Foo extends Component {
function Rate() {
  let params = useParams();

  const [rating, setRating] = useState(0);
  let isloggin = localStorage.getItem("Login");
  let token = localStorage.getItem("Token");
  let auth = localStorage.getItem("Auth");
  useEffect(() => {
    axios
      .get("http://localhost/laravel/laravel/public/api/blog/rate/" + params.id)
      .then((response) => {
        if (isloggin) {
          let sum = 0;
          let tbc = 0;
          let obj = Object.keys(response.data.data).length;
          Object.keys(response.data.data).map(function (key, index) {
            let rate = response.data.data[key].rate;
            sum = sum + rate;
          });
          tbc = sum / obj;
          setRating(tbc);
        } else {
          setRating(0);
        }
      })
      .catch((error) => console.log(error));
  }, []);
  // Tong rate/so người rate (length)
  function changeRating(newRating, name) {
    //   this.setState({
    //     rating: newRating
    //   });
    setRating(newRating);
    //   xu ly tiep tai day

    if (isloggin) {
      token = JSON.parse(token);
      auth = JSON.parse(auth);
      let url =
        "http://localhost/laravel/laravel/public/api/blog/rate/" + params.id;

      let config = {
        headers: {
          Authorization: "Bearer " + token,
          "Content-Type": "application/x-www-form-urlencoded",
          Accept: "application/json",
        },
      };

      const formData = new FormData();
      formData.append("blog_id", +params.id);
      formData.append("user_id", auth.id);
      formData.append("rate", newRating);
      axios.post(url, formData, config).then((res) => {
        alert("You have rated this item successfully");
      });
    } else {
      alert("Please login");
      setRating(0);
    }
  }

  // rating = 2;
  return (
    <>
      <span>RATE THIS ITEM:</span>
      <StarRatings
        rating={rating}
        starRatedColor="blue"
        changeRating={changeRating}
        numberOfStars={5}
        name="rating"
      />
    </>
  );
}

export default Rate;
