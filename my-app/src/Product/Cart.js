import axios from "axios";
import { useEffect, useState } from "react";

function Cart() {
  const [cart, setCart] = useState("");
  let item = localStorage.getItem("Item");
  item = JSON.parse(item);
  useEffect(() => {
    axios
      .post("http://localhost/laravel/laravel/public/api/product/cart", item)
      .then((res) => {
        setCart(res.data.data);
      });
  }, []);

  function renderItem() {
    if (cart) {
      return cart.map((value, key) => {
        let img = value.image;
        img = JSON.parse(img);
        return (
          <tr>
            <td>
              <img
                className="media-object"
                src={
                  "http://localhost/laravel/laravel/public/upload/user/product/" +
                  value.id_user +
                  "/" +
                  img[0]
                }
                alt=""
                style={{ width: 150 }}
              />
            </td>
            <td>
              {value.name}
              <br />
              Wed ID: {value.id}
            </td>
            <td> ${value.price}</td>
            <td class="cart_quantity">
              <div class="cart_quantity_button">
                <a
                  id={value.id}
                  href=""
                  class="cart_quantity_up"
                  onClick={handleAdd}
                >
                  +
                </a>
                <input
                  class="cart_quantity_input"
                  type="text"
                  name="quantity"
                  value={value.qty}
                  autocomplete="off"
                  size="2"
                />
                <a
                  id={value.id}
                  href=""
                  class="cart_quantity_down"
                  onClick={handleMinus}
                >
                  -
                </a>
              </div>
            </td>
            <td class="cart_total">
              <p class="cart_total_price">$ {value.price * value.qty}</p>
            </td>
            <td>
              <a class="cart_quantity_delete">
                <i
                  id={value.id}
                  class="fa-sharp fa-solid fa-delete-left"
                  onClick={handleRemove}
                ></i>
              </a>
            </td>
          </tr>
        );
      });
    }
  }

  function handleAdd(e) {
    e.preventDefault();
    const id = e.target.id;
    let cart2 = [...cart];
    if (cart2) {
      return cart2.map((value1) => {
        if (value1.id == id) {
          value1.qty += 1;
          setCart(cart2);
          {
            Object.keys(item).map((value2, key) => {
              if (value2 == id) {
                item[value2] = value1.qty;
                localStorage.setItem("Item", JSON.stringify(item));
              }
            });
          }
        }
      });
    }
  }
  function handleMinus(e) {
    e.preventDefault();
    const id = e.target.id;
    let cart2 = [...cart];
    if (cart2) {
      return cart2.map((value1, key1) => {
        if (value1.id == id) {
          if (value1.qty > 1) {
            value1.qty -= 1;
            setCart(cart2);

            Object.keys(item).map((value2, key) => {
              if (value2 == id) {
                item[value2] = value1.qty;
                localStorage.setItem("Item", JSON.stringify(item));
              }
            });
          } else {
            delete cart2[key1];
            setCart(cart2);

            Object.keys(item).map((key, index) => {
              if (key === id) {
                delete item[key];
                localStorage.setItem("Item", JSON.stringify(item));
              }
            });
          }
        }
      });
    }
  }
  function handleRemove(e) {
    const id = e.target.id;
    let sub_cart = [...cart];
    if (sub_cart) {
      return sub_cart.map((value, key) => {
        if (value.id == id) {
          delete sub_cart[key];
          setCart(sub_cart);
          Object.keys(item).map((key1, index) => {
            if (key1 === id) {
              delete item[key1];
              localStorage.setItem("Item", JSON.stringify(item));
            }
          });
        }
      });
    }
  }
  return (
    <section id="cart_items">
      <div class="col-sm-9">
        <div class="table-responsive cart_info">
          <table id="mytable" class="table table-condensed">
            <thead>
              <tr class="cart_menu">
                <td class="image">Item</td>
                <td class="description"></td>
                <td class="price">Price</td>
                <td class="quantity">Quantity</td>
                <td class="total">Total</td>
                <td></td>
              </tr>
            </thead>
            <tbody>{renderItem()}</tbody>
          </table>
        </div>
      </div>
    </section>
  );
}
export default Cart;
