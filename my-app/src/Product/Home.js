import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { UserContext } from "../UserContext";

function Home() {
  const [products, setProducts] = useState([]);
  const context = useContext(UserContext);
  let picture = "";
  let [sub_qty, setSub_qty] = useState(0);
  const [wishlist, setWishlist] = useState([]);
  const [wishlistitem, setWishlistitem] = useState("");
  let item = localStorage.getItem("Item");
  item = JSON.parse(item);
  useEffect(() => {
    axios
      .get("http://localhost/laravel/laravel/public/api/product")
      .then((res) => {
        setProducts(res.data.data);
      });
    axios
      .get("http://localhost/laravel/laravel/public/api/product/wishlist")
      .then((res) => {
        setWishlistitem(res.data.data);
      });
  }, []);
  function renderProducts() {
    return (
      products.length > 0 &&
      products.map((key, index) => {
        picture = key.image;
        picture = JSON.parse(picture);
        return (
          <div class="col-sm-4">
            <div class="product-image-wrapper">
              <div class="single-products">
                <div class="productinfo text-center">
                  <img
                    src={
                      "http://localhost/laravel/laravel/public/upload/user/product/" +
                      key.id_user +
                      "/" +
                      picture[0]
                    }
                    alt=""
                    style={{ width: "100px" }}
                  />
                  <h2>${key.price}</h2>
                  <p>{key.name}</p>
                  <a href="#" class="btn btn-default add-to-cart">
                    <i class="fa fa-shopping-cart"></i>Add to cart
                  </a>
                </div>
                <div class="product-overlay">
                  <div class="overlay-content">
                    <h2>${key.price}</h2>
                    <p>{key.name}</p>
                    <a
                      href="#"
                      class="btn btn-default add-to-cart"
                      id={key.id}
                      onClick={addCarthome}
                    >
                      <i class="fa fa-shopping-cart"></i>
                      Add to cart
                    </a>
                    <br />
                    <a class="btn btn-default add-to-cart">
                      <i class="fa-solid fa-circle-info"></i>
                      <Link to={"/product/detail/" + key.id}>
                        <span style={{ color: "black" }}>More</span>
                      </Link>
                    </a>
                  </div>
                </div>
              </div>
              <div class="choose">
                <ul class="nav nav-pills nav-justified">
                  <li>
                    <a id={key.id} href="" onClick={Wishlist}>
                      <i class="fa fa-plus-square"></i>Add to wishlist
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-plus-square"></i>Add to compare
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        );
      })
    );
  }
  function addCarthome(e) {
    e.preventDefault();
    const id = +e.target.id;
    setSub_qty((sub_qty += 1));
    let sum = 1;
    if (item) {
      Object.keys(item).map((key, value) => {
        key = Number(key);
        item[key] = Number(item[key]);
        sum = sum + item[key];
        context.cartContext(sum);
        if (key == id) {
          item[key] += 1;
          localStorage.setItem("Item", JSON.stringify(item));
        }
      });
    }
  }

  function Wishlist(e) {
    let flag = true;
    e.preventDefault();

    const id = e.target.id;
    const push = wishlistitem.filter(function (hello) {
      return hello.id == id;
    });
    push.map((value, key) => {
      wishlist.push(value);
    });
    context.moving(wishlist.length);
    let list = localStorage.getItem("wishlist");
    list = JSON.parse(list);
    if (list) {
      let object = Object.assign({}, list, wishlist);
      localStorage.setItem("wishlist", JSON.stringify(object));
    } else {
      localStorage.setItem("wishlist", JSON.stringify(push));
    }
    alert("Successfully");
  }
  return (
    <div class="features_items">
      <h2 class="title text-center">Features Items</h2>
      {renderProducts()}
    </div>
  );
}
export default Home;
